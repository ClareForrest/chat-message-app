# Real time chat app
Example of chat web application using Socket.IO with the MERN stack Client in React, Server in Node.js/Express 

Designed to save each conversation history in MongoDB with CRUD functionality on both the conversation history and on individual messages within that conversation.

# Set up
Clone repository in target path
$ git clone 
Install dependencies
$ node install

# Create a .env file in the server folder with the following data;
NODE_ENV=development
PORT=3000
SOCKET_PORT=5001
MONGODB_URL=<mongodb_url>

# Run in local development
npm start


# File Structure
**.env** file holds private keys etc and is in .gitignore
**config.js** reads from .env file and exports the keys with their relative values
**express.js** file requires express and exports express set up as app module
**server.js** file requires in app module from ./express and also express as a standalone for separate reference in the Public folder 
**socket.js** creates the socket connection and exports as an ioConnection module to other files
**server.js** is where the mongoose connection is set up, along with headers and base file path
**index.js** requires in functions and holds all the routes. They are exported as a router module




# Websites of Reference
- https://socket-io-website.vercel.app/docs/v4 
- https://socket.io/get-started/private-messaging-part-1/
- https://github.com/socketio/socket.io/tree/master/examples/private-messaging
