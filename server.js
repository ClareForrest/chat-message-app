const mongoose = require('mongoose');
const app = require('./utils/express');
const config = require('./config/config');
const express = require('express');
const fs = require('fs');
require('dotenv').config();
const Message = require('./controllers/message_controller')
const ioConnection = require('./utils/socket');

mongoose.connect(
	config.MONGODB_URI,
	{
		useNewUrlParser: true,
		useFindAndModify: false,
		useCreateIndex: true, 
		useUnifiedTopology: true,
	},
	(err) => {
		if (err) {
			console.log('error connecting mongodb -- ❌');
			console.log(err.message);
		} else {
			console.log('connected to mongodb -- ✅');
		}
	}
);

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*") 
  res.header(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  )
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  )
  res.header("Access-Control-Allow-Credentials", true)
  res.header("Connection", "Keep-Alive")
  res.header("Keep-Alive", "timeout=150, max=1000")
  next()
})

app.get('/', (req, res) => {
	// return res.send('welcome to the chatapp');
	res.sendFile(__dirname + '/index.html'); //Link front and backends, then move html to front end
});

//pull in functions, ie getMessage, postMessage to the index 
//and pass to the http request as an argument

//******* Currently Not Working *******/
app.use('/', require('./routes/index'))
app.use(express.static('./public'))
app.post('/uploadDocument', (req, res) => {
	res.send("test")
})
app.listen(config.PORT, () => {
	console.log(`listening on PORT: ${config.PORT}`);
});

// Ultimately want to move to utils/socket.js ??
let socketId = ''
ioConnection.on('connection', async (socket) => {
	// persist session 
	socketId = socket.id

	// emit session details 

	
	//socket.on(eventName, callback)
  socket.on('chat message', async (getMessages) => {
		try {
			const msg = await Message.model.create({
				user_name: 'Clare Test',
				message: msg,
			})
			return message.id
		} catch (error) {
			console.error(error.message)
		}
	})
	
});

		// sending to individual socketid (private message) 
// 		io.to(socketId).emit('chat message', msg);
//   };
// 	socket.on('disconnect', () => {
// 		console.log('user disconnected', socket.id);
// 	});
// });
//io.close() to call the create/save chatHistory function 



module.exports = app;
