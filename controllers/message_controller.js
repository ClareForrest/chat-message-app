const Message = require('../models/message');
const ObjectId = require('mongodb').ObjectID;
const ioConnection = require('../utils/socket')	

//******* Currently Not Working *******/
// const getMessages = async (req, res) => {
// 	return await Message
// 		.find()
// 		.sort({ _id: -1 })
// 		.limit(10)
// 		.then((res) => {
// 			socket.emit('Messages', res.reverse());
// 		})
// }

//******* Currently Not Working *******/
const createMessage = (req, res) => {
	Message.model.create()	
	.then(() => {
		ioConnection.emit('newChatMessage', {
			user_name: res.user_name,
			message: res.message,
		}); //will need some error handling in here
	})
};

module.exports = {
	// getMessages,
	createMessage,
}
