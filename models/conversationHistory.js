const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Message = require('./message');

const conversationHistorySchema = new Schema(
	{
		// Thinking this would default to the _id, but allow the user option to rename
		conversation_name: String,
		required: true,
	  message_id: {
		  type: mongoose.Schema.Types.ObjectId,
	  	ref: 'Message'}
    },
	{ timestamps: true }
);

module.exports = {
	model: mongoose.model('ConversationHistory', conversationHistorySchema),
	schema: conversationHistorySchemas
};
