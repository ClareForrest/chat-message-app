const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const messageSchema = new Schema(
	{
		user_name: {
			type: String,
			// required: true, //commented out during set up phase
		},
		message: {
			type: String,
			required: true,
		},
	},
	{ timestamps: true }
);

module.exports = {
	model: mongoose.model('Message', messageSchema),
	schema: messageSchema,
};
