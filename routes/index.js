// Use for storing routes
const express = require('express');
const router = express.Router();
const {
  getMessages,
  createMessage,
  updateMessage,
  deleteMessage
} = require('../controllers/message_controller')
 
// test route
router.get('/', (req, res) => res.send('Test GET route'));

//******* Currently Not Working *******/
// router.get('/messages', getMessages)
// router.post('/message', createMessage)
// router.put('/updatemessage/:id', updateMessage) //not sure if this or best to have this in conversationHistory
// router.delete('/deletemessage/:id', deleteMessage)

module.exports = router;
