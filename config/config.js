require('dotenv').config();

module.exports = {
  NODE_ENV: process.env.NODE_ENV,
  MONGODB_URI: process.env.MONGODB_URI,
  ENDPOINT: process.env.API_URL,
  MASTERKEY: process.env.API_KEY,
  PORT: process.env.PORT,
  SOCKET_PORT: process.env.SOCKET_PORT
};